require('isomorphic-fetch');
require('es6-promise').polyfill();

const express = require('express');
const app = express();
const api = require('./api/');

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/', (req, res) => {
  res.send('Hello World!');
});

/**
  Simple flight search api wrapper.

  TODO: client should provide params

  Api params and location values are here:
  http://business.skyscanner.net/portal/en-GB/Documentation/FlightsLivePricingQuickStart
*/
app.get('/api/search', (req, res) => {
  api.livePricing.search(req.query)
  .then((results) => {
    const agents = {};
    const legs = {};
    const carriers = {};
    const places = {};

    const response = [];

    for (let agent of results.Agents) {
      agents[agent.Id] = agent;
    }

    for(let leg of results.Legs) {
      legs[leg.Id] = leg;
    }

    for(let carrier of results.Carriers) {
      carriers[carrier.Id] = carrier;
    }

    for(let place of results.Places) {
      places[place.Id] = place;
    }

    for(let itinerary of results.Itineraries ) {
      const resultNode = {
        agentName: null,
        price: null,
        id: null,
        inboundArrival: null,
        inboundCarrierLogo: null,
        inboundCarrierName: null,
        inboundDeparture: null,
        inboundStops: null,
        inboundDuration: null,
        inboundOriginStation: null,
        inboundDestinationStation: null,
        outboundArrival: null,
        outboundCarrierLogo: null,
        outboundCarrierName: null,
        outboundDeparture: null,
        outboundStops: null,
        outboundDuration: null,
        outboundOriginStation: null,
        outboundDestinationStation: null,
      };

      resultNode.inboundArrival = legs[itinerary.InboundLegId].Arrival;
      resultNode.inboundDeparture = legs[itinerary.InboundLegId].Departure;
      resultNode.inboundStops = legs[itinerary.InboundLegId].Stops.length;
      resultNode.inboundDuration = legs[itinerary.InboundLegId].Duration;
      resultNode.inboundOriginStation = places[legs[itinerary.InboundLegId].OriginStation].Code;
      resultNode.inboundDestinationStation = places[legs[itinerary.InboundLegId].DestinationStation].Code;
      resultNode.inboundCarrierLogo = carriers[legs[itinerary.InboundLegId].Carriers[0]].ImageUrl;
      resultNode.inboundCarrierName = carriers[legs[itinerary.InboundLegId].Carriers[0]].Name;

      resultNode.outboundArrival = legs[itinerary.OutboundLegId].Arrival;
      resultNode.outboundDeparture = legs[itinerary.OutboundLegId].Departure;
      resultNode.outboundStops = legs[itinerary.OutboundLegId].Stops.length;
      resultNode.outboundDuration = legs[itinerary.OutboundLegId].Duration;
      resultNode.outboundOriginStation = places[legs[itinerary.OutboundLegId].OriginStation].Code;
      resultNode.outboundDestinationStation = places[legs[itinerary.OutboundLegId].DestinationStation].Code;
      resultNode.outboundCarrierLogo = carriers[legs[itinerary.OutboundLegId].Carriers[0]].ImageUrl;
      resultNode.outboundCarrierName = carriers[legs[itinerary.OutboundLegId].Carriers[0]].DisplayCode;

      for( let pricingOption of itinerary.PricingOptions ) {
        let pricingNode = JSON.parse(JSON.stringify(resultNode));
        pricingNode.price = pricingOption.Price;
        pricingNode.agentName = agents[pricingOption.Agents[0]].Name;
        pricingNode.id = response.length;
        response.push(pricingNode);
      }
    }

    res.json(response);
  })
  .catch(console.error);
});

app.listen(4000, () => {
  console.log('Node server listening on http://localhost:4000');
});
