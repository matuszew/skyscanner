import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';

import TopNav from './common/components/TopNav/TopNav';
import { Search } from './Search/container/connect';

import './App.scss';

export const App = ({
  store,
}) => (
  <Provider store={store}>
    <div className="App">
      <TopNav />
      <Search />
    </div>
  </Provider>
);

App.propTypes = {
  store: PropTypes.object.isRequired,  // eslint-disable-line
};
