import React from 'react';
import ReactDOM from 'react-dom';

import { createRootStore } from "./common/store/rootStore";

import { App } from './App';

import './common/styles/main.scss';

const store = createRootStore({});

ReactDOM.render(
  <App store={store} />,
  document.getElementById('root'),
);
