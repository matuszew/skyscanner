import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import styles from './styles.scss';

export const Button = ({
  children,
  className,
  label,
  onClick,
}) => (
  <button
    className={classnames(styles.Button, className)}
    onClick={onClick}
  >
    {label}
    {children}
  </button>
);

Button.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  label: PropTypes.string,
  onClick: PropTypes.func.isRequired,
};

Button.defaultProps = {
  children: undefined,
  label: '',
  className: '',
};
