import React from 'react';
import logo from '../../../common/assets/logo.svg';
import iconMenu from '../../../common/assets/icon-menu.svg';
import { Button } from '../../../common/components/Button/Button';

import styles from './styles.scss';

const TopNav = () => (
  <header className={styles.header}>
    <a href="/">
      <span className={styles.logoText}>Skyscanner</span>
      <img className={styles.logo} alt="Skyscanner" src={logo} />
    </a>
    <Button className={styles.ButtonHamburger} onClick={() => { }}>
      <img alt="menu" src={iconMenu} />
    </Button>
  </header>
);

export default TopNav;
