import moment from 'moment';

export const formatDate = (dateString, format) => moment(dateString)
  .format(format);
