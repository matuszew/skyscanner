import moment from 'moment';
import momentDurationFormatSetup from 'moment-duration-format';

momentDurationFormatSetup(moment);

export const formatDuration = (duration, format) => moment.duration(duration, "minutes").format(format);

