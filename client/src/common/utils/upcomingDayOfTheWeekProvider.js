import moment from 'moment';

export const provideUpcomingDay = (dayOfTheWeek) => {
  const today = moment().isoWeekday();

  if (today < dayOfTheWeek) {
    return moment().isoWeekday(dayOfTheWeek);
  } else {
    return moment().add(1, 'weeks').isoWeekday(dayOfTheWeek);
  }
};

