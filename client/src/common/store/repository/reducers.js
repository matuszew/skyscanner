import reducerChain from 'reducer-chain';
import get from 'lodash/get';
import clone from 'lodash/clone';
import setWith from 'lodash/setWith';

import {
  FETCH_FAILED,
  FETCH_INIT,
  FETCH_SUCCESS,
} from './actions';

const initialRepository = {};
const initialRepositoryState = {
  data: null,
  loading: false,
  success: false,
  error: false,
};

export const set = (state, namespace, value) =>
  setWith(clone(state), namespace, value, clone);

export const repositoryReducer = (state = initialRepositoryState, action) => {
  switch (action.type) {
    case FETCH_INIT:
      return {
        ...state,
        data: undefined,
        loading: true,
        error: false,
        success: false,
      };
    case FETCH_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false,
        error: false,
        success: true,
      };
    case FETCH_FAILED:
      return {
        ...state,
        data: null,
        loading: false,
        error: action.payload,
        success: false,
      };
    default:
      return state;
  }
};

export const reducer = (reducers = []) => (state = initialRepository, action) => {
  const {
    namespace,
  } = action;

  if (!namespace) {
    return state;
  }

  const namespaceState = get(state, namespace);
  const newNamespaceState = repositoryReducer(namespaceState, action);

  return reducers.length
    ? set(state, namespace, reducerChain(reducers)(newNamespaceState, action))
    : set(state, namespace, newNamespaceState);
};
