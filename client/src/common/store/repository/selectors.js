import { createSelector } from 'reselect';
import { get } from 'lodash';

import { DATA_KEY, LOADING_KEY } from './consts';

const getNamespaceState = namespace => state => get(state.repository, namespace);

const getNamespaceKeyValue = (key, defaultValue) => namespaceState => get(namespaceState, key, defaultValue);

export const getData = (namespace, defaultValue) => createSelector(
  getNamespaceState(namespace),
  namespaceState => getNamespaceKeyValue(DATA_KEY, defaultValue)(namespaceState),
);

export const isDataFetching = namespace => createSelector(
  getNamespaceState(namespace),
  namespaceState => getNamespaceKeyValue(LOADING_KEY)(namespaceState) === true,
);
