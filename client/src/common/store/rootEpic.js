import { createEpicMiddleware, combineEpics } from 'redux-observable';

import { repositoryEpics } from './repository/middlewares';

export const applicationEpics = combineEpics(repositoryEpics);

export const createRootEpic = createEpicMiddleware();
