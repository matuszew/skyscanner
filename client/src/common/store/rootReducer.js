import { combineReducers } from 'redux';

import { reducer as repositoryReducer } from './repository/reducers';

const repository = repositoryReducer([]);

const appReducer = combineReducers({
  repository,
});

export const rootReducer = (state, action) => appReducer(state, action);
