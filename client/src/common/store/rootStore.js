import { applyMiddleware, createStore } from 'redux';

import { composeWithDevTools } from 'redux-devtools-extension'; // eslint-disable-line

import { rootReducer } from './rootReducer';
import { applicationEpics, createRootEpic } from './rootEpic';

export const createRootStore = (initialState = {}) => {
  const store = createStore(
    rootReducer,
    initialState,
    composeWithDevTools(applyMiddleware(createRootEpic)),
  );

  createRootEpic.run(applicationEpics);

  return store;
};

