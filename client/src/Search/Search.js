import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import priceAlerts from '../common/assets/price-alerts.svg';

import { formatDate } from "../common/utils/dateFormatter";
import { Spinner } from "../common/components/Spinner/Spinner";
import SearchHeader from './SearchHeader/SearchHeader';
import SearchHeading from './SearchHeader/SearchHeading/SearchHeading';
import SearchDescription from './SearchHeader/SearchDescription/SearchDescription';
import { SearchControls } from './SearchControls/SearchControls';
import { SearchControl } from './SearchControls/SearchControl/SearchControl';
import { SearchResults } from './SearchResults/SearchResults';
import { SearchResult } from './SearchResults/SearchResult/SearchResult';
import { ResultFlightOption } from './SearchResults/SearchResult/ResultFlightOption/ResultFlightOption';
import { ResultSummary } from './SearchResults/SearchResult/ResultSummary/ResultSummary';

import styles from './styles.scss';
import { formatDuration } from "../common/utils/durationFormatter";

const SEARCH_TIME_FORMAT = 'H:mm';
const SEARCH_DURATION_FORMAT = 'h[h] m';

export class Search extends Component {
  componentDidMount() {
    this.props.fetchAvailableFlights();
  }

  render() {

    const {
      isAvailableFlightsLoading,
      availableFlights,
    } = this.props;

    if(isAvailableFlightsLoading) return <Spinner/>;
    if(!availableFlights) return <div> No flights available </div>

    return (
      <Fragment>
        <SearchHeader>
          <SearchHeading queryFrom="EDI" queryTo="LON" />
          <SearchDescription>
            2 travellers, economy
          </SearchDescription>
        </SearchHeader>
        <SearchControls>
          <SearchControl onClick={() => {}}>
            Filter
          </SearchControl>
          <SearchControl onClick={() => {}}>
            Sort
          </SearchControl>
          <SearchControl onClick={() => {}}>
            <img
              alt="Price Alerts"
              className={styles.SearchControl_Icon}
              src={priceAlerts}
            />
            Price alerts
          </SearchControl>
        </SearchControls>
        <SearchResults>
          { availableFlights.map(({
            agentName,
            id,
            inboundArrival,
            inboundCarrierLogo,
            inboundCarrierName,
            inboundDeparture,
            inboundDestinationStation,
            inboundDuration,
            inboundOriginStation,
            inboundStops,
            outboundArrival,
            outboundCarrierLogo,
            outboundCarrierName,
            outboundDeparture,
            outboundDestinationStation,
            outboundDuration,
            outboundOriginStation,
            outboundStops,
            price
                                  }) => (
            <SearchResult key={id}>
              <ResultFlightOption
                arrivalTime={formatDate(outboundArrival, SEARCH_TIME_FORMAT)}
                carrierLogo={outboundCarrierLogo}
                carrierName={outboundCarrierName}
                departureTime={formatDate(outboundDeparture, SEARCH_TIME_FORMAT)}
                destinationStation={outboundDestinationStation}
                flightDuration={formatDuration(outboundDuration, SEARCH_DURATION_FORMAT)}
                originStation={outboundOriginStation}
                stopsCount={outboundStops}
              />
              <ResultFlightOption
                arrivalTime={formatDate(inboundArrival, SEARCH_TIME_FORMAT)}
                carrierLogo={inboundCarrierLogo}
                carrierName={inboundCarrierName}
                departureTime={formatDate(inboundDeparture, SEARCH_TIME_FORMAT)}
                destinationStation={inboundDestinationStation}
                flightDuration={formatDuration(inboundDuration, SEARCH_DURATION_FORMAT)}
                originStation={inboundOriginStation}
                stopsCount={inboundStops}/>
              <ResultSummary
                agentName={agentName}
                onClick={() => {}}
                price={price}/>
            </SearchResult>
            ))
          }
        </SearchResults>
      </Fragment>
    );
  }
}

Search.propTypes = {
  availableFlights: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  isAvailableFlightsLoading: PropTypes.bool.isRequired,
  fetchAvailableFlights: PropTypes.func.isRequired,
};
