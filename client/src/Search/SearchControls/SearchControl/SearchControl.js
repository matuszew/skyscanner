import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import styles from './styles.scss';

export const SearchControl = ({
  children,
  className,
}) => (
  <div className={classnames(styles.SearchControl, className)}>
    {children}
  </div>
);

SearchControl.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  onClick: PropTypes.func.isRequired,
};

SearchControl.defaultProps = {
  className: '',
};
