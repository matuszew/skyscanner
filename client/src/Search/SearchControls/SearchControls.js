import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import styles from './styles.scss';

export const SearchControls = ({
  children,
  className,
}) => (
  <div className={classnames(styles.SearchControls, className)}>
    {children}
  </div>
);

SearchControls.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
};

SearchControls.defaultProps = {
  className: '',
};
