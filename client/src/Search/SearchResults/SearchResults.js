import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import styles from './styles.scss';

export const SearchResults = ({
  children,
  className,
}) => (
  <div className={classnames(styles.SearchResults, className)}>
    {children}
  </div>
);

SearchResults.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
};

SearchResults.defaultProps = {
  className: '',
};
