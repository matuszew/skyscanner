import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import longArrowRight from '../../../../common/assets/long-arrow-right-grey.svg';

import styles from './styles.scss';

export const ResultFlightOption = ({
  arrivalTime,
  carrierLogo,
  carrierName,
  className,
  departureTime,
  destinationStation,
  flightDuration,
  originStation,
  stopsCount,
}) => {
  const hasStops = stopsCount > 0;
  const stopsLabel = hasStops ? `${stopsCount} stops` : 'Direct';

  const stopsClasses = classnames(
    styles.ResultFlightOption_Type,
    {
      [styles.ResultFlightOption_Type__hasStops]: hasStops,
    },
  );

  return (
    <div className={classnames(styles.ResultFlightOption, className)}>
      <img className={styles.ResultFlightOption_Logo} src={carrierLogo} alt={carrierName}/>
      <div className={styles.ResultFlightOption_FlightDetailsWrapper}>
        <div className={styles.ResultFlightOption_DetailsNodeWrapper}>
          <span className={styles.ResultFlightOption_Hour}>{departureTime}</span>
          <span className={styles.ResultFlightOption_Direction}>{originStation}</span>
        </div>
        <img
          alt="arrow_right"
          className={styles.ResultFlightOption_Divider}
          src={longArrowRight}
        />
        <div className={styles.ResultFlightOption_DetailsNodeWrapper}>
          <span className={styles.ResultFlightOption_Hour}>{arrivalTime}</span>
          <span className={styles.ResultFlightOption_Direction}>{destinationStation}</span>
        </div>
      </div>
      <div className={styles.ResultFlightOption_DetailsWrapper}>
        <span className={styles.ResultFlightOption_Duration}>{flightDuration}</span>
        <span className={stopsClasses}>{stopsLabel}</span>
      </div>
  </div>
  )
};

ResultFlightOption.propTypes = {
  arrivalTime: PropTypes.string.isRequired,
  carrierLogo: PropTypes.string.isRequired,
  carrierName: PropTypes.string.isRequired,
  className: PropTypes.string,
  departureTime: PropTypes.string.isRequired,
  destinationStation: PropTypes.string.isRequired,
  flightDuration: PropTypes.string.isRequired,
  originStation: PropTypes.string.isRequired,
  stopsCount: PropTypes.number.isRequired,
};

ResultFlightOption.defaultProps = {
  className: '',
};
