import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { Button } from '../../../../common/components/Button/Button';

import styles from './styles.scss';

export const ResultSummary = ({
  agentName,
  className,
  onClick,
  price,
}) => (
  <div className={classnames(styles.ResultSummary, className)}>
    <div className={styles.ResultSummary_PriceWrapper}>
      <h3 className={styles.ResultSummary_Price}>£{price}</h3>
      <p className={styles.ResultSummary_PriceSource}>{agentName}</p>
    </div>
    <Button
      className={styles.ResultSummary_SelectButton}
      label="Select"
      onClick={onClick}
    />
  </div>
);

ResultSummary.propTypes = {
  agentName: PropTypes.string.isRequired,
  className: PropTypes.string,
  onClick: PropTypes.func.isRequired,
  price: PropTypes.number.isRequired,
};

ResultSummary.defaultProps = {
  className: '',
};
