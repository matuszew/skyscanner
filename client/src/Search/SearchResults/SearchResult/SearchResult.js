import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import styles from './styles.scss';

export const SearchResult = ({
  children,
  className,
}) => (
  <div className={classnames(styles.SearchResult, className)}>
    {children}
  </div>
);

SearchResult.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
};

SearchResult.defaultProps = {
  className: '',
};
