import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import longArrowRight from '../../../common/assets/long-arrow-right.svg';

import styles from './styles.scss';

const SearchHeading = ({
  className,
  queryFrom,
  queryTo,
}) => (
  <div className={classnames(styles.SearchHeading, className)}>
    {queryFrom}
    <img
      alt="arrow_right"
      className={styles.SearchHeading_ArrowDivider}
      src={longArrowRight}
    />
    {queryTo}
  </div>
);

SearchHeading.propTypes = {
  className: PropTypes.string,
  queryFrom: PropTypes.string,
  queryTo: PropTypes.string,
};

SearchHeading.defaultProps = {
  className: '',
  queryFrom: '',
  queryTo: '',
};

export default SearchHeading;
