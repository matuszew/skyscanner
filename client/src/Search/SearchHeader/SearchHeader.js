import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import styles from './styles.scss';

const SearchHeader = ({
  children,
  className,
}) => (
  <div className={classnames(styles.SearchHeader, className)}>
    {children}
  </div>
);

SearchHeader.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
};

SearchHeader.defaultProps = {
  className: '',
};

export default SearchHeader;
