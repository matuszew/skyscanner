import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import styles from './styles.scss';

const SearchDescription = ({
  children,
  className,
}) => (
  <div className={classnames(styles.SearchDescription, className)}>
    {children}
  </div>
);

SearchDescription.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
};

SearchDescription.defaultProps = {
  className: '',
};

export default SearchDescription;
