import { fetchInit } from '../../common/store/repository/actions';
import { ENDPOINT } from '../../common/consts/endpoints';
import { NAMESPACE } from '../../common/consts/namespaces';

export const fetchAvailableFlights = query => fetchInit(
  ENDPOINT.SEARCH,
  NAMESPACE.HOMEPAGE,
  {
    query,
  },
);
