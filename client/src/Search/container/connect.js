import { connect } from 'react-redux';
import moment from 'moment';
import { Search as SearchView } from '../Search';
import { fetchAvailableFlights } from './actions';
import { isDataFetching } from '../../common/store/repository/selectors';
import { NAMESPACE } from '../../common/consts/namespaces';
import { getAvailableFlights } from './selectors';
import { provideUpcomingDay } from "../../common/utils/upcomingDayOfTheWeekProvider";

const MONDAY = 1;
const SEARCH_QUERY_DATE_FORMAT= 'YYYY-MM-DD';

const mapStateToProps = state => ({
  isAvailableFlightsLoading: isDataFetching(NAMESPACE.HOMEPAGE)(state),
  availableFlights: getAvailableFlights(state),
});

const mapDispatchToProps = dispatch => ({
  fetchAvailableFlights: payload => dispatch(fetchAvailableFlights({
    adults: 2,
    class: 'Economy',
    fromDate: moment(provideUpcomingDay(MONDAY)).format(SEARCH_QUERY_DATE_FORMAT),
    fromPlace: 'EDI-sky',
    toDate: moment(provideUpcomingDay(MONDAY)).add(1, 'day').format(SEARCH_QUERY_DATE_FORMAT),
    toPlace: 'LOND-sky',
  })),
});

export const Search = connect(mapStateToProps, mapDispatchToProps)(SearchView);
