import { createSelector } from 'reselect';
import { NAMESPACE } from '../../common/consts/namespaces';
import { getData } from '../../common/store/repository/selectors';

export const getAvailableFlights = createSelector(
  getData(NAMESPACE.HOMEPAGE, []),
  data => data,
);
